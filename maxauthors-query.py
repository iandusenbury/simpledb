import boto3
import pdb


client = boto3.client('sdb')

selectAll = 'select * from `paper`'

response = client.select(
    SelectExpression=selectAll,
    NextToken=' ',
    ConsistentRead=True
)

items = response # holds all our items from select statement
# need this to get all pages from simpleDB
while('NextToken' in response):
    nextToken = response['NextToken']
    response = client.select(
        SelectExpression=selectAll,
        NextToken=nextToken,
        ConsistentRead=True
    )

    items['Items'].extend(response['Items'])


papers = []
author_name = []
maxauthors = [{'author_count': 0}] #temporary variable for checks

for paper in items['Items']:
    # get all author ids in paper
    author_ids = []
    for attr in paper['Attributes']:
        if attr['Name'] == 'author':
            author_ids.append(attr['Value'])

    num_authors = len(author_ids)

    # get author names from author domain
    response = ''
    if num_authors >= maxauthors[0]['author_count']:
        sql = 'SELECT name FROM `authors` WHERE itemName() IN (%s)' % str(author_ids)[1:-1]
        response = client.select(
            SelectExpression=sql,
            NextToken=' ',
            ConsistentRead=True
        )
        response = response['Items']

    # author object
    author = {
        'id': paper['Name'],
        'author_count': num_authors,
        'authors': response,
        'title': paper['Attributes'][-1]['Value']
    }

    # if we find a paper with the same number of coauthors we want to
    # return both papers
    if num_authors == maxauthors[0]['author_count']:
        maxauthors.append(author)

    # if it's the only one then we just return the paper
    if num_authors > maxauthors[0]['author_count']:
        maxauthors = [author]

# maxauthors = max(papers, key=lambda x:x['author_count'])
print(maxauthors)
