import xml.etree.ElementTree as ET
import lxml
import boto3
import pdb
import csv

client = boto3.client('sdb')
domain = 'books'

response = client.delete_domain(
    DomainName=domain
)

response = client.create_domain(
    DomainName=domain
)

with open('seeddata/books_small.csv') as infile:
    reader = csv.reader(infile)
    next(reader) # ignore header row
    for row in reader:
        book_id     = row[0]
        book_title  = row[1]
        author_id   = row[2]
        author_name = row[3]

        response = client.put_attributes(
            DomainName = domain,
            ItemName = book_id,
            Attributes = [
              {
                  'Name': 'title',
                  'Value': book_title,
                  'Replace': True
              }, {
                  'Name': 'author',
                  'Value': author_id,
                  'Replace': False
              }, {
                  'Name': 'author_name',
                  'Value': author_name,
                  'Replace': False
              }
            ]
        )

        print(response)
