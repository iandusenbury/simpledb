import xml.etree.ElementTree as ET
import lxml
import boto3
import pdb
import csv

client = boto3.client('sdb')

response = client.delete_domain(
    DomainName='proc-test'
)

response = client.create_domain(
    DomainName='proc-test'
)

with open('seeddata/proceedings.csv') as infile:
    reader = csv.reader(infile)
    next(reader) # ignore header row
    for row in reader:
        proc_id    = row[0]
        proc_title = row[1]
        paper_id   = row[2]

        response = client.put_attributes(
            DomainName = 'proc-test',
            ItemName = proc_id,
            Attributes = [
              {
                  'Name': 'title',
                  'Value': proc_title,
                  'Replace': True
              }, {
                  'Name': 'paper',
                  'Value': paper_id,
                  'Replace': False
              }
            ]
        )

        print(response)
