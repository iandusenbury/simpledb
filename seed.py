import xml.etree.ElementTree as ET
import lxml
import boto3
import pdb


client = boto3.client('sdb')
xml = 'dblp-data.xml'

object_ids = []

for event, elem in ET.iterparse(open(xml), events=('start', 'end')):
    if elem.tag == 'OBJECT' and event == 'start':
        # pdb.set_trace()
        for key, value in elem.attrib.items():
            object_ids.append(value)
        # print(elem.tag, elem.attrib)
    if elem.tag == 'LINK':
        break
    elem.clear()

for obj in object_ids:
    print(obj)

# domains
www = []
articles = []
incollections = []
books = []
authors = []

# everything below this line is old code that needs to be
# converted to use the new parser
#
# for s in soup:
#     domain = s.name
#     contents = s.contents
#     attrib = []
#
#     attrib.append(
#       {
#         'Name': 'type',
#         'Value': s.name,
#         'Replace': False
#       }
#     )
#
#     attrib.append(
#       {
#         'Name': 'mdate',
#         'Value': s['mdate'],
#         'Replace': False
#       }
#     )
#
#     attrib.append(
#       {
#         'Name': 'key',
#         'Value': s['key'],
#         'Replace': False
#       }
#     )
#
#     for item in contents:
#         a = {
#           'Name': item.name,
#           'Value': item.string,
#           'Replace': False
#         }
#         attrib.append(a)
#
#     item = {
#         'Name': str(i),
#         'Attributes': attrib
#     }
#
#     items.append(item)
#     i += 1
#
# print(items)
#
# response = client.batch_put_attributes(
#   DomainName='article',
#   Items=articles
# )
#
# print(response)
