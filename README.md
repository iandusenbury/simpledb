# README #

Seed and Query simpledb for dblp

### Dependencies ###

* python 3.6.1

### Setup ###

Make sure you have python 3.6.1 installed

clone this repo `git clone git@bitbucket.org:iandusenbury/simpledb.git`

[Download dblp database](http://kdl.cs.umass.edu/databases/dblp-data.xml.gz) and uncompress file

place xml file into the root of the project.

now we're ready to setup our environment:


```
cd simpledb
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
aws configure
```
was configuration details are in the slack channel

now we can run our test to see if we are connected to simple db

```
python test-simpledb-connection.py
```