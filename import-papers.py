import xml.etree.ElementTree as ET
import lxml
import boto3
import pdb
import csv

client = boto3.client('sdb')
domain = 'paper'

response = client.delete_domain(
    DomainName=domain
)

response = client.create_domain(
    DomainName=domain
)

with open('seeddata/papers_small.csv') as infile:
    reader = csv.reader(infile)
    next(reader) # ignore header row
    for row in reader:
        # print(row)
        paper_id    = row[0]
        paper_title = row[1]
        author_id   = row[2]
        author_name = row[3]

        # print(paper_id, paper_title, author_id, author_name, '\n')

        response = client.put_attributes(
            DomainName = domain,
            ItemName = paper_id,
            Attributes = [
              {
                  'Name': 'title',
                  'Value': paper_title,
                  'Replace': True
              }, {
                  'Name': 'author',
                  'Value': author_id,
                  'Replace': False
              }
            ]
        )

        print(response, paper_id)
