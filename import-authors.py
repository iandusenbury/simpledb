import xml.etree.ElementTree as ET
import lxml
import boto3
import pdb
import csv


client = boto3.client('sdb')
domain = 'authors'

response = client.delete_domain(
    DomainName=domain
)

response = client.create_domain(
    DomainName=domain
)

with open('seeddata/authors_small.csv') as infile:
    reader = csv.reader(infile)
    next(reader) # ignore header row
    for row in reader:
        author_id   = row[0]
        author_name = row[1]
        work_id     = row[2]
        work_type   = row[3]

        author = {
          'Name': author_id,
          'Attributes': [
            {
                'Name': 'name',
                'Value': author_name,
                'Replace': True
            }, {
                'Name': work_type,
                'Value': work_id,
                'Replace': False
            }
          ]
        }

        response = client.put_attributes(
            DomainName = 'authors',
            ItemName = author_id,
            Attributes = [
              {
                  'Name': 'name',
                  'Value': author_name,
                  'Replace': True
              }, {
                  'Name': work_type,
                  'Value': work_id,
                  'Replace': False
              }
            ]
        )

        print(response)
